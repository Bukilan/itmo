#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>


int main()
{
	printf("Pointer\n");

	char arr[4];
	char *ptr_arr = arr;
	*ptr_arr = 'W';
	*(ptr_arr + 1) = 'O';
	*(ptr_arr + 2) = 'R';
	*(ptr_arr + 3) = 'K';
	
	printf("%c ", *(ptr_arr));
	printf("%c ", *(ptr_arr + 1));
	printf("%c ", *(ptr_arr + 2));
	printf("%c ", *(ptr_arr + 3));

	printf("\n");
	puts("Dynamic memory");

	char *ptr_int;
	if (ptr_int != NULL)
	{
		ptr_int = (char*)malloc(4*sizeof(char));
	} else
	{
		puts("Memory allocation error");
	}
	ptr_int[0] = 'W';
	ptr_int[1] = 'O';
	ptr_int[2] = 'R';
	ptr_int[3] = 'K';
	printf("%c ", ptr_int[0]);
	printf("%c ", ptr_int [1]);
	printf("%c ", ptr_int [2]);
	printf("%c ", ptr_int [3]);
	free(ptr_int);
	return 0;
}

