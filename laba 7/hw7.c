#include <stdio.h>
#include <math.h>

enum Directions { CLASSIC = 1,POP,RAP,ROCK,NEWADGE,DUBSTEP,HIPHOP,TRAP};

typedef struct
{
	float x1;
	float x2;
	float y1;
	float y2;
	float vector_x;
	float vector_y;
} LineSeg;

static union {
    unsigned short command;
    struct {
        unsigned short DSL: 1;
        unsigned short PPP: 1;
        unsigned short LINK: 1;
    } ADSL;
};

int main()
{
	printf("%s%d\n","Music direction:", ROCK);


	LineSeg a;
	printf("Input 1'st x':");
	scanf_s("%f", &a.x1);
	printf("Input 1'st y':");
	scanf_s("%f", &a.y1);
	printf("Input 2'nd' x':");
	scanf_s("%f", &a.x2);
	printf("Input 2'nd y':");
	scanf_s("%f", &a.y2);
	a.vector_x = a.x2 - a.x1;
	a.vector_y = a.y2 - a.y1;
	printf("%s %f %s %f","x = ", a.vector_x,"y = ", a.vector_y);
	
	printf("3. Input new command: ");
    scanf("%x", &command);
    printf("   DSL is %s\n", (ADSL.DLS == 1) ? ("ON") : ("OFF"));
    printf("   PPP is %s\n", (ADSL.PPP == 1) ? ("ON") : ("OFF"));
    printf("   LINK is %s\n", (ADSL.LINK == 1) ? ("ON") : ("OFF"));
    return 0;
}

