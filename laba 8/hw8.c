#include <stdio.h>
#include <string.h>

int main (void){
	int n;
    char *f_string;
    char *s_string;
    char symbol;
    f_string = (char*) malloc(39);
    s_string = (char*) malloc(39);
    if (f_string == NULL) or (s_string == NULL)
    {
    	puts("Memory allocation error");
    	exit(2);	
	}
	
	// 5'th task
	puts("Copy 1'st string in 2'nd");
    puts("Input 1'st string");
    fgets( f_string );
    puts("Input 2'nd string");
    fgets( s_string );
    puts("Result: ");
    puts( strcpy( f_string , s_string ) );
	
	// 7'th task
	puts("Take lenhgt of string");
    puts("Input 1'st string");
    fgets( f_string );
    puts("Result: ");
    printf ("legth of string %s = %d \n", f_string, strlen (f_string));
    
    // 9'th task
    
    puts("Search symbol in string");
    puts("Input string");
    fgets( f_string );
    puts("Input symbol");
    scanf("%c", &symbol);
    char *ach;
    ach=strrchr (f_string,symbol);
    //getchar();
    puts("Result is:");
    printf("%d\n" , ach-f_string+1);
	
	// 11'th task
	puts("Find length of second string line that included in first");
    puts("Input 1'st string");
    fgets( f_string );
    puts("Input 2'nd string");
    fgets( s_string );
    puts("Result is:");
    int num=strspn( f_string , s_string );
    printf( "%d\n" , num );
    
    // 13'th task 
    
    puts("Input first string: ");
    fgets(f_string);
    f_string[strlen(f_string) - 1] = '\0'; //delete 'enter' from end
    puts("Input second string: ");
    fgets(s_string);
    strcpy(f_string, strpbrk(f_string, s_string));
    f_string[1] = '\0';
    int tokens = strtok(f_string,s_string);
    printf("The string is divided by a separator \"%s\":\n");
    while (tokens) {
        printf("      %s\n", tokens);
        tokens = strtok(NULL, s_string);
    }
	free(f_string);
	free(s_string);
	return 0;
}

