#include <stdio.h>
#include <string.h>

int main(void){

    // Task �1

    int number = 0,
            uppercase = 0,
            lowercase = 0;
    char array [50] = {'\0'};

    puts ("Task �1");
    puts ("Input string");
    gets (array);

    for (int i = 0; i < 50 ; i++) {
        if (array[i] >= '0' && array[i] <= '9') number++;
        if (array[i] >= 'a' && array[i] <= 'z') lowercase++;
        if (array[i] >= 'A' && array[i] <= 'Z') uppercase++;
	}

    printf ( "Numbers: %d\n" , number);
    printf ( "Lowercase: %d\n" , lowercase);
    printf ( "Uppercase: %d\n" , uppercase);

    // Task 5
    
	float i_amount;
    float percent;
    int time;
    
    printf("Input initial amount: ");
     scanf("%f", &i_amount);
    printf("Input annual percents: ");
     scanf("%f", &percent);
    printf("Input deposit time: ");
     scanf("%d", &time);
     
    int month;
    for (month = 0; month < time; month++) {
        i_amount =  (i_amount * (1 + percent / 100.0));
        printf("%d: %.2f\n",month+1, i_amount);
    }
    return 0;
} 
